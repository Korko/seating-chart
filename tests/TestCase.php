<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\Assertions\ArrayTestCaseTrait;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, ArrayTestCaseTrait;
}
