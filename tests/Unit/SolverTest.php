<?php

namespace Tests\Unit;

use App\Solvers\Data\Guest;
use App\Solvers\Solver;
use Exception;
use Illuminate\Support\Arr;
use Tests\TestCase;

class SolverTest extends TestCase
{
    // Check for more guests than places
    public function test_no_solution_overpopulated(): void
    {
        $alice = new Guest('Alice');
        $bob = new Guest('Bob');
        $charlie = new Guest('Charlie');
        $guests = [$alice, $bob, $charlie];

        // Only 2 places but 3 guests
        // TODO: This should be a custom exception
        $this->expectException(Exception::class);
        Solver::solve($guests, [2]);
    }

    // Check for respect of the table minimum occupancy rate
    public function test_no_solution_underpopulated(): void
    {
        $alice = new Guest('Alice');
        $bob = new Guest('Bob');
        $charlie = new Guest('Charlie');
        $guests = [$alice, $bob, $charlie];

        // 4 places but 3 guests and all places should be assigned!
        // TODO: This should be a custom exception
        $this->expectException(Exception::class);
        Solver::solve($guests, [4], 1);
    }

    // Check for a simple 3 guests / 1 table solution, no preferences
    public function test_simplest_solution(): void
    {
        $alice = new Guest('Alice');
        $bob = new Guest('Bob');
        $charlie = new Guest('Charlie');
        $guests = [$alice, $bob, $charlie];

        $solution = Solver::solve($guests, [3]);
        $this->assertTables([$guests], $solution);
    }

    // Check for a simple 4 guests / 2 table solution, no preferences
    public function test_simpler_solution(): void
    {
        $alice = new Guest('Alice');
        $bob = new Guest('Bob');
        $charlie = new Guest('Charlie');
        $david = new Guest('David');
        $guests = [$alice, $bob, $charlie, $david];

        $solution = Solver::solve($guests, [2, 2]);

        // 2 table only and all the guests are there
        $this->assertEquals(2, count($solution));
        $this->assertArraySimilar($guests, array_merge($solution[0]->guests, $solution[1]->guests));
    }

    // Check apart condition, no solution
    public function test_no_solution_apart_condition(): void
    {
        $alice = new Guest('Alice');
        $bob = new Guest('Bob');
        $charlie = new Guest('Charlie');
        $guests = [$alice, $bob, $charlie];

        // No solution possible, only 1 table but 2 guests have to be apart
        $alice->willBeFarFrom($bob);

        $this->expectException(Exception::class);
        Solver::solve($guests, [3]);
    }

    // Check for a simple 4 guests / 2 table solution, with exclusions (only one solution possible)
    public function test_simple_apart_condition(): void
    {
        $alice = new Guest('Alice');
        $bob = new Guest('Bob');
        $charlie = new Guest('Charlie');
        $david = new Guest('David');
        $guests = [$alice, $bob, $charlie, $david];

        // The only solution should be [Alice, Charlie] and [Bob, David]
        $alice->willBeFarFrom($bob);
        $bob->willBeFarFrom($charlie);

        $solution = Solver::solve($guests, [2, 2]);

        $this->assertTables([[$alice, $charlie], [$bob, $david]], $solution);
    }

    // Check close condition, no solution
    public function test_no_solution_close_condition(): void
    {
        $alice = new Guest('Alice');
        $bob = new Guest('Bob');
        $charlie = new Guest('Charlie');
        $david = new Guest('David');
        $guests = [$alice, $bob, $charlie, $david];

        // No solution possible, only 2 place per table but 3 guests have to be together
        $alice->willBeCloseTo($bob);
        $bob->willBeCloseTo($charlie);

        $this->expectException(Exception::class);
        Solver::solve($guests, [2, 2]);
    }

    // Check for a simple 4 guests / 2 table solution, with requirements (only one solution possible)
    public function test_simple_close_condition(): void
    {
        $alice = new Guest('Alice');
        $bob = new Guest('Bob');
        $charlie = new Guest('Charlie');
        $david = new Guest('David');
        $guests = [$alice, $bob, $charlie, $david];

        // The only solution should be [Alice, Bob] and [Charlie, David]
        $alice->willBeCloseTo($bob);

        $solution = Solver::solve($guests, [2, 2]);

        $this->assertTables([[$alice, $bob], [$charlie, $david]], $solution);
    }

    // Check for a simple 4 guests / 2 table solution, with requirements and exclusions (only one solution possible)
    public function test_simple_close_and_apart_condition(): void
    {
        $alice = new Guest('Alice');
        $bob = new Guest('Bob');
        $charlie = new Guest('Charlie');
        $david = new Guest('David');
        $erin = new Guest('Erin');
        $guests = [$alice, $bob, $charlie, $david, $erin];

        // The only solution should be [Alice, David] and [Charlie, Bob]
        // But without the far condition, [Alice, David] and any other guest would have been valid too
        $alice->willBeCloseTo($david);
        $alice->willBeFarFrom($bob);
        $alice->willBeFarFrom($charlie);
        $bob->willBeCloseTo($erin);

        // 3 guests per table with a minimum occupancy rate of 50% => 2 guests per table minimum
        $solution = Solver::solve($guests, [3, 3], .5);

        $this->assertTables([[$alice, $david], [$bob, $charlie, $erin]], $solution);
    }

    /**
     * Check if two arrays are similar, regardless of the order of their elements
     * @param array<array<Guest>> $expected
     * @param array<Table> $actual
     */
    protected function assertTables(array $expected, array $actual): void
    {
        $this->assertEquals(count($expected), count($actual));

        foreach ($actual as $actualTable) {
            $errorMessage = "The table {$actualTable} is not in the list of expected ones: ".implode(', ', array_map(fn($table) => '[ '.implode(', ', array_map(fn($guest) => $guest->name, $table)).' ]', $expected));
            $this->assertNotNull(Arr::first($expected, fn($expectedGuests) => Arr::isLike($expectedGuests, $actualTable->guests)), $errorMessage);
        }
    }
}
