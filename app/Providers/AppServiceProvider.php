<?php

namespace App\Providers;

use App\Solvers\SeatingPlanSolver;
use App\Solvers\Solver;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        App::bind('solver', function () {
            return App::make(SeatingPlanSolver::class);
        });

        /**
         * Checks if an array contains a value
         * @param array $array
         * @param mixed $value
         * @return bool
         */
        Arr::macro('contains', function ($array, $value) : bool {
            return in_array($value, $array, true);
        });

        /**
         * Checks if an array contains all the values of another array
         * @param array $array
         * @param array $values
         * @return bool
         */
        Arr::macro('containsAll', function ($array, $values) : bool {
            // returns true if there's no value in $values that cannot be found in $array
            return Arr::first($values, fn($value) => ! Arr::contains($array, $value)) === null;
        });

        /**
         * Checks if an array contains any of the values of another array
         * @param array $array All the possible values
         * @param array $values The actual values
         * @return bool
         */
        Arr::macro('containsAny', function ($array, $values) : bool {
            // returns true if there's a value in $values that can be found in $array
            return Arr::first($values, fn($value) => Arr::contains($array, $value)) !== null;
        });

        /**
         * Checks if an array contains all the values of another array and vice versa, not caring about order
         * @param array $array
         * @param array $values
         * @return bool
         */
        Arr::macro('isLike', function ($array, $values) : bool {
            return Arr::containsAll($array, $values) && Arr::containsAll($values, $array);
        });
    }
}
