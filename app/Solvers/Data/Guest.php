<?php

namespace App\Solvers\Data;

use Illuminate\Support\Arr;

class Guest
{
    public GuestPreferences $preferences;
    public Table $table;

    public function __construct(
        public string $name
    )
    {
        $this->preferences = new GuestPreferences();
    }

    public function willBeCloseTo(Guest|array $guest): void
    {
        $this->preferences->willBeCloseTo($guest);
        $guest->preferences->willBeCloseTo($this);
    }

    public function shouldBeCloseTo(Guest|array $guest): void
    {
        $this->preferences->shouldBeCloseTo($guest);
        $guest->preferences->shouldBeCloseTo($this);
    }

    public function shouldBeFarFrom(Guest|array $guest): void
    {
        $this->preferences->shouldBeFarFrom($this, $guest);
        $guest->preferences->shouldBeFarFrom($this);
    }

    public function willBeFarFrom(Guest|array $guest): void
    {
        $this->preferences->willBeFarFrom($guest);
        $guest->preferences->willBeFarFrom($this);
    }

    public function cannotBeFarFrom(): array
    {
        return $this->preferences->closeTo;
    }

    public function prefersToBeCloseTo(): array
    {
        return $this->preferences->betterCloseTo;
    }

    public function prefersToBeFarFrom(): array
    {
        return $this->preferences->betterFarFrom;
    }

    public function cannotBeCloseTo(): array
    {
        return $this->preferences->farFrom;
    }

    public function tableJudgement(Table $table): int
    {
        return Arr::containsAny($table->guests, $this->cannotBeCloseTo()) ? -2 :
            (Arr::containsAny($table->guests, $this->prefersToBeFarFrom()) ? -1 :
            (Arr::containsAny($table->guests, $this->prefersToBeCloseTo()) ? 1 :
            (Arr::containsAny($table->guests, $this->cannotBeFarFrom()) ? 2 : 0)));
    }

    public function willSitAtTable(Table $table): void
    {
        if (isset($this->table)) {
            $this->table->removeGuest($this);
        }

        $this->table = $table;
        $this->table->addGuest($this);
    }
}
