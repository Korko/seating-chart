<?php

namespace App\Solvers\Data;

class GuestPreferences
{
    public array $closeTo;
    public array $betterCloseTo;
    public array $farFrom;
    public array $betterFarFrom;

    public function __construct()
    {
        $this->closeTo = [];
        $this->betterCloseTo = [];
        $this->betterFarFrom = [];
        $this->farFrom = [];
    }

    private function addGuestToList(Guest|array $guest, array &$array): void
    {
        if(is_array($guest)) {
            $array = array_merge($array, $guest);
        } else {
            $array[] = $guest;
        }
    }

    public function willBeCloseTo(Guest|array $guest): void
    {
        $this->addGuestToList($guest, $this->closeTo);
    }

    public function shouldBeCloseTo(Guest|array $guest): void
    {
        $this->addGuestToList($guest, $this->betterCloseTo);
    }

    public function shouldBeFarFrom(Guest|array $guest): void
    {
        $this->addGuestToList($guest, $this->betterFarFrom);
    }

    public function willBeFarFrom(Guest|array $guest): void
    {
        $this->addGuestToList($guest, $this->farFrom);
    }
}
