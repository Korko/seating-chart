<?php

namespace App\Solvers\Data;

class Table
{
    public array $guests;

    public function __construct(
        public int $size
    )
    {
        $this->guests = [];
    }

    public function addGuest(Guest $guest): void
    {
        $this->guests[] = $guest;
    }

    public function removeGuest(Guest $guest): void
    {
        unset($this->guests[array_search($guest, $this->guests)]);
    }

    public function __toString() : string
    {
        return '[ '.implode(', ', array_map(fn($guest) => $guest->name, $this->guests)).' ]';
    }
}
