<?php

namespace App\Solvers;

use App\Solvers\Data\Guest;
use App\Solvers\Data\Table;
use Illuminate\Support\Arr;

class SeatingPlanSolver
{
    /**
     * Solve a seating plan
     *
     * @param array<Guest> $guests List of the guests (names)
     * @param array<int> $tableSizes List of tables sizes (number of seats)
     *
     * @return array<array<Guest>> List of tables, each table being a list of guests one next to the other
     */
    public function solve(array $guests, array $tableSizes, float $minTableOccupancyRate = 1): array
    {
        if(count($guests) < round(array_sum($tableSizes) * $minTableOccupancyRate)) {
            throw new \Exception('Not enough guests to fill the tables');
        }

        $tables = array_map(fn($tableSize) => new Table($tableSize), $tableSizes);

        // Shuffle the guests to avoid always getting the same solution and sort them by the number of constraints they have
        shuffle($guests);
        usort($guests, fn($a, $b) => count($a->cannotBeFarFrom()) + count($a->cannotBeCloseTo()) <=> count($b->cannotBeFarFrom()) + count($b->cannotBeCloseTo()));
        $guests = array_reverse($guests);

        if(! $this->placeGuests($guests, $tables, $minTableOccupancyRate)) {
            throw new \Exception('No solution found');
        }

        return $tables;
    }

    private function placeGuests(array $guests, array $tables, float $minTableOccupancyRate): bool
    {
        if(count($guests) === 0) {
            // Check that every table is full enough
            return count(array_filter($tables, fn ($table) => count($table->guests) < $table->size * $minTableOccupancyRate)) === 0;
        }

        // Recursive function, take the first guest and try every table
        $guest = Arr::first(array_splice($guests, 0, 1));

        return $this->placeGuest($guest, $guests, $tables, $minTableOccupancyRate);
    }

    private function placeGuest(Guest $guest, array $guests, array $tables, float $minTableOccupancyRate): bool
    {
        $validTables = $this->filterAndOrderTables($guest, $tables);
        shuffle($validTables);

        // Try every table and if it fails, try the next one
        // If it works, returns true
        foreach($validTables as $table) {
            try {
                $guest->willSitAtTable($table);
                if ($this->placeGuests($guests, $tables, $minTableOccupancyRate)) {
                    return true;
                }
            } catch (\Exception $e) {
                continue;
            }
        }

        return false;
    }

    private function filterAndOrderTables(Guest $guest, array $tables): array
    {
        // If some tables have people that the guest have to be close to, only select them
        $favoriteTables = array_filter($tables, fn($table) => Arr::containsAny($table->guests, $guest->cannotBeFarFrom()));
        if(count($favoriteTables) > 0) {
            $tables = $favoriteTables;
        }

        // Remove tables that are full or that the guest cannot sit at
        $tables = array_filter($tables, fn($table) => ( count($table->guests) < $table->size && ! Arr::containsAny($table->guests, $guest->cannotBeCloseTo()) ));

        // Order the tables by preference and occupancy
        usort($tables, function($a, $b) use ($guest) {
            return $guest->tableJudgement($a) <=> $guest->tableJudgement($b) ?: count($a->guests) <=> count($b->guests);
        });

        return $tables;
    }
}
