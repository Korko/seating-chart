<?php

namespace App\Solvers;

use Illuminate\Support\Facades\Facade;

class Solver extends Facade
{
    /**
     * Get the registered name of the component.
     */
    protected static function getFacadeAccessor(): string
    {
        return 'solver';
    }
}
